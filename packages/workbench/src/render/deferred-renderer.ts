import type { LC, PropsWithChildren, LiveElement } from '@use-gpu/live';
import type { UseGPURenderContext } from '@use-gpu/core';
import type { LightEnv, PassFlags, RenderComponents } from '../pass/types';

import { use, yeet, memo, useMemo, useOne } from '@use-gpu/live';

import { PassReconciler } from '../reconcilers/index';

import { DebugRender } from './forward/debug';
import { PickingRender } from './forward/picking';
import { ShadedRender } from './forward/shaded';
import { ShadowRender } from './forward/shadow';
import { SolidRender } from './forward/solid';
import { UIRender } from './forward/ui';

import { useStandardBindGroups } from '../pass/bindings';

import { DeferredPass } from '../pass/deferred-pass';

import { DeferredShadedRender } from './deferred/shaded';
import { DeferredSolidRender } from './deferred/solid';
import { DeferredUIRender } from './deferred/ui';

import { Renderer } from './renderer';
import { LightRender } from './light/light-render';
import { LightMaterial } from './light/light-material';

const {quote} = PassReconciler;

const DEFAULT_PASSES = [
  use(DeferredPass, {}),
];

const NO_BUFFERS: Record<string, UseGPURenderContext[]> = {};
const NO_FLAGS: DeferredRendererFlags = {};

export type DeferredRendererFlags = Pick<PassFlags, 'shadows' | 'merge' | 'overlay'>;

export type DeferredRendererProps = PropsWithChildren<{
  buffers?: Record<string, UseGPURenderContext[]>,
  flags?: DeferredRendererFlags,
  passes?: LiveElement[],
  components?: RenderComponents,
}>;

const getComponents = ({modes = {}, renders = {}}: Partial<RenderComponents>): RenderComponents => {
  return {
    modes: {
      debug: DebugRender,
      picking: PickingRender,
      shadow: ShadowRender,
      ...modes,
    },
    renders: {
      solid: {opaque: DeferredSolidRender, transparent: SolidRender},
      shaded: {opaque: DeferredShadedRender, transparent: ShadedRender},
      ui: {opaque: DeferredUIRender, transparent: UIRender},
      ...renders,
    }
  }
};

export const DeferredRenderer: LC<DeferredRendererProps> = memo((props: DeferredRendererProps) => {
  const {
    buffers = NO_BUFFERS,
    flags = NO_FLAGS,
    passes = DEFAULT_PASSES,

    children,
  } = props;

  const {
    overlay = false,
    merge = false,
    shadows = !!buffers.shadow,
  } = flags;

  const components = useOne(() => getComponents(props.components ?? {}), props.components);

  // Provide forward-lit material + emit deferred light draw calls
  const view = use(LightMaterial, {
    shadows,
    children,
    then: (light: LightEnv) =>
      useMemo(() => quote([
        yeet({ env: { light }}),
        use(LightRender, {...light, shadows}),
        // eslint-disable-next-line react-hooks/exhaustive-deps
      ]), [light, shadows]),
  });

  // Pass bindings
  const bindGroups = useStandardBindGroups(flags);

  return Renderer({ buffers, bindGroups, children: view, components, passes, overlay, merge });
}, 'DeferredRenderer');
