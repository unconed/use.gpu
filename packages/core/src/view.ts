import type { ViewUniforms } from './types';

import { mat4, vec2, vec3, vec4 } from 'gl-matrix';

export const makeViewUniforms = (): ViewUniforms => ({
  projectionViewFrustum: { current: [vec4.create(), vec4.create(), vec4.create(), vec4.create(), vec4.create(), vec4.create()] },

  projectionViewMatrix: { current: mat4.create() },
  projectionMatrix: { current: mat4.create() },
  viewMatrix: { current: mat4.create() },

  inverseProjectionViewMatrix: { current: mat4.create() },
  inverseProjectionMatrix: { current: mat4.create() },
  inverseViewMatrix: { current: mat4.create() },

  viewPosition: { current: vec4.create() },
  viewNearFar: { current: vec2.create() },
  viewResolution: { current: vec2.create() },
  viewSize: { current: vec2.create() },
  viewWorldDepth: { current: vec2.fromValues(1, 1) },
  viewPixelRatio: { current: 1 },
});

export const updateViewSize = (
  uniforms: ViewUniforms,
  width: number,
  height: number,

  dpi?: number,
  worldScale?: number,
  zbiasScale?: number,
) => {
  const {
    viewSize,
    viewResolution,
    viewPixelRatio,
    viewWorldDepth,
  } = uniforms;

  if (width != null && height != null) {
    viewSize.current = vec2.fromValues(width, height);
    viewResolution.current = vec2.fromValues(1 / width, 1 / height);
  }

  viewPixelRatio.current = dpi ?? 1;
  viewWorldDepth.current = vec2.fromValues(worldScale ?? 1, zbiasScale ?? 1);
};

export const updateViewProjection = (uniforms: ViewUniforms, projection?: mat4, view?: mat4, position?: vec4, near?: number, far?: number) => {
  const {
    viewMatrix,
    viewNearFar,
    viewPosition,
    projectionMatrix,
    projectionViewMatrix,
    projectionViewFrustum,
    inverseViewMatrix,
    inverseProjectionMatrix,
    inverseProjectionViewMatrix,
  } = uniforms;

  if (projection) projectionMatrix.current = projection;
  if (view) viewMatrix.current = view;

  mat4.multiply(projectionViewMatrix.current, projectionMatrix.current, viewMatrix.current);
  projectionViewFrustum.current = makeFrustumPlanes(projectionViewMatrix.current);

  mat4.invert(inverseViewMatrix.current, viewMatrix.current);
  mat4.invert(inverseProjectionMatrix.current, projectionMatrix.current);
  mat4.invert(inverseProjectionViewMatrix.current, projectionViewMatrix.current);

  if (position) viewPosition.current = position;
  else {
    viewPosition.current[0] = 0;
    viewPosition.current[1] = 0;
    viewPosition.current[2] = 0;
    viewPosition.current[3] = 1;
    vec3.transformMat4(viewPosition.current as vec3, viewPosition.current as vec3, inverseViewMatrix.current);
  }

  if (near != null && far != null) {
    viewNearFar.current = vec2.fromValues(near, far);
  }
};

const REVERSE_Z = mat4.create();
mat4.translate(REVERSE_Z, REVERSE_Z, vec3.fromValues(0, 0, 1));
mat4.scale(REVERSE_Z, REVERSE_Z, vec3.fromValues(1, 1, -1));

export const reverseZ = (a: mat4, b: mat4) => mat4.multiply(a, REVERSE_Z, b);

export const makeOrthogonalMatrix = (
  left: number,
  right: number,
  top: number,
  bottom: number,
  near: number,
  far: number,
): mat4 => {
  const x = 2 / (right - left);
  const y = 2 / (bottom - top);
  const z = -1 / (far - near);

  const wx = -(right + left) / (right - left);
  const wy = -(bottom + top) / (bottom - top);
  const wz = -(near) / (far - near);

  const matrix = mat4.create();
  mat4.set(matrix,
    x, 0, 0, 0,
    0, y, 0, 0,
    0, 0, z, 0,
    wx, wy, wz, 1,
  );

  return matrix;
}

export const makeProjectionMatrix = (
  width: number,
  height: number,
  fov: number,
  near: number,
  far: number,
  radius: number = 1,
  dolly: number = 1,
): mat4 => {
  const aspect = width / height;
  let matrix;

  if (dolly === 1) {
    // Normal WebGPU perspective matrix
    matrix = mat4.create();
    mat4.perspectiveZO(matrix, fov, aspect, near, far);

    // Move Z from 0..1 to 1..0 in clip space (reversed Z)
    reverseZ(matrix, matrix);
  }
  else if (dolly > 0) {
    const shift = (1 / dolly - 1) * radius;
    const dFov = Math.atan(Math.tan(fov / 2) * dolly) * 2;
    const dNear = near + shift;
    const dFar = far + shift;

    // WebGPU perspective matrix with reduced FOV and shifted near/far plane
    matrix = mat4.create();
    mat4.perspectiveZO(matrix, dFov, aspect, dNear, dFar);

    // Move Z from 0..1 to 1..0 in clip space (reversed Z)
    reverseZ(matrix, matrix);
  }
  else {
    // Orthogonal matrix
    const s = radius * Math.tan(fov / 2);
    matrix = makeOrthogonalMatrix(-aspect * s, aspect * s, -s, s, far, near);
  }

  return matrix;
}

const NO_TARGET = [0, 0, 0];

export const makeOrbitMatrix = (
  radius: number,
  phi: number,
  theta: number,
  target: number[] | vec3 | vec4 = NO_TARGET,
  dolly: number = 1,
): mat4 => {

  const matrix = mat4.create();
  mat4.translate(matrix, matrix, vec3.fromValues(0, 0, -radius / (dolly || 1)));
  mat4.rotate(matrix, matrix, theta, vec3.fromValues(1, 0, 0));
  mat4.rotate(matrix, matrix, phi, vec3.fromValues(0, 1, 0));

  const t = [-target[0], -target[1], -target[2]] as any as vec3;
  mat4.translate(matrix, matrix, t);

  return matrix;
}

export const makeOrbitPosition = (
  radius: number,
  phi: number,
  theta: number,
  target: number[] | vec3 | vec4 = NO_TARGET,
  dolly: number = 1,
): vec4 => {
  const ct = Math.cos(theta);
  radius /= Math.max(1e-5, dolly);
  return vec4.fromValues(
    -Math.sin(phi) * ct * radius + (target[0] || 0),
    Math.sin(theta) * radius + (target[1] || 0),
    Math.cos(phi) * ct * radius + (target[2] || 0),
    1,
  );
}

export const makePanMatrix = (x: number, y: number, zoom: number, dolly: number): mat4 => {
  const matrix = mat4.create();
  mat4.translate(matrix, matrix, vec3.fromValues(x, y, 1 - 1 / (dolly || 1)));
  mat4.scale(matrix, matrix, vec3.fromValues(zoom, zoom, zoom));
  return matrix;
}

export const makePanPosition = (x: number, y: number, zoom: number, dolly: number): vec4 => {
  const z = 1 - 1 / Math.max(1e-5, dolly);
  return vec4.fromValues(x, y, z, 1);
}

export const makeFrustumPlanes = (m: mat4): vec4[] => {
  const out = [];

  out.push(vec4.fromValues(m[3] - m[0], m[7] - m[4], m[11] - m[8], m[15] - m[12]));
  out.push(vec4.fromValues(m[3] + m[0], m[7] + m[4], m[11] + m[8], m[15] + m[12]));

  out.push(vec4.fromValues(m[3] - m[1], m[7] - m[5], m[11] - m[9], m[15] - m[13]));
  out.push(vec4.fromValues(m[3] + m[1], m[7] + m[5], m[11] + m[9], m[15] + m[13]));

  out.push(vec4.fromValues(m[3] - m[2], m[7] - m[6], m[11] - m[10], m[15] - m[14]));
  out.push(vec4.fromValues(       m[2],        m[6],         m[10],         m[14]));

  for (const v of out) vec4.scale(v, v, 1/Math.hypot(v[0], v[1], v[2]));

  return out;
};

export const distanceToFrustum = (frustum: vec4[], x: number, y: number, z: number): number => {
  let min = Infinity;
  for (const v of frustum) {
    const d = v[0]*x + v[1]*y + v[2]*z + v[3];
    min = Math.min(min, d);
  }
  return min;
};
